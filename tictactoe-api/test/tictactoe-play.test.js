const request = require('supertest');
const { expect } = require('chai');

const app = require('../src/app');

describe('POST /api/tic-tac-toe/play', () => {
  const post = payload => request(app).post('/api/tic-tac-toe/play').send(payload);

  it('sends invalid payload without matchId', done => {
    post({ empty: false })
      .expect(403)
      .then(({ body }) => {
        expect(body).to.have.property('error');
        expect(body).to.have.property('message');
        expect(body.message).to.be.equal('Not valid matchId');
        done();
      })
      .catch(done);
  });

  it('sends invalid payload with bad matchId', done => {
    post({ matchId: '-' })
      .expect(403)
      .then(({ body }) => {
        expect(body).to.have.property('error');
        expect(body).to.have.property('message');
        expect(body.message).to.be.equal('Not valid matchId');
        done();
      })
      .catch(done);
  });

  it('sends empty payload to start a game', done => {
    post({})
      .expect(200)
      .then(({ body }) => {
        expect(body).to.have.property('matchId');
        expect(body).to.have.property('boardState');
        expect(body.boardState).to.have.lengthOf(9);

        done();
      });
  });

  const match = () => {
    return post({})
      .expect(200)
      .then(({ body: { matchId, boardState } }) => {
        const aiFirst = boardState.find(state => state !== '-');
        return { matchId, boardState, aiFirst };
      });
  };

  it('validate random initial board state on match creation', done => {
    match()
      .then(({ boardState, aiFirst }) => {
        if (aiFirst) {
          expect(boardState.filter(state => state === '-')).to.have.lengthOf(8);
          expect(boardState.filter(state => state === aiFirst)).to.have.lengthOf(1);
        } else {
          expect(boardState.filter(state => state === '-')).to.have.lengthOf(9);
        }
        done();
      })
      .catch(done);
  });

  it('create initial move for front player on random position', done => {
    const marks = { user: ['x', 'o'][randomIndex(0, 1)] };
    let isAiFirst = false;
    match()
      .then(({ matchId, boardState, aiFirst }) => {
        if (aiFirst) {
          isAiFirst = true;
          marks.ai = aiFirst;
          marks.user = aiFirst === 'x' ? 'o' : 'x';
          const idx = boardState.findIndex(state => state === aiFirst);
          let userIdx;
          while (!userIdx) {
            const tmpIdx = randomIndex(0, 8);
            if (tmpIdx !== idx) userIdx = tmpIdx;
          }
          return post({ matchId, boardState, nextMove: { char: marks.user, position: userIdx } });
        }

        const idx = randomIndex(0, 8);
        marks.ai = marks.user === 'x' ? 'o' : 'x';
        return post({ matchId, boardState, nextMove: { char: marks.user, position: idx } });
      })
      .then(req => {
        const { body } = req;
        expect(req.statusCode).to.be.equal(200);
        if (body && body.boardState) {
          const { boardState } = body;
          if (isAiFirst) {
            expect(boardState.filter(state => state === '-')).to.have.lengthOf(6);
            expect(boardState.filter(state => state === marks.ai)).to.have.lengthOf(2);
            expect(boardState.filter(state => state === marks.user)).to.have.lengthOf(1);
          } else {
            expect(boardState.filter(state => state === '-')).to.have.lengthOf(7);
            expect(boardState.filter(state => state === marks.ai)).to.have.lengthOf(1);
            expect(boardState.filter(state => state === marks.user)).to.have.lengthOf(1);
          }
        }

        done();
      })
      .catch(done);
  });

  it('check invalid move trying to cheat with same mark as CPU', done => {
    const marks = { user: 'x' };
    match()
      .then(({ matchId, boardState, aiFirst }) => {
        if (aiFirst) {
          marks.ai = aiFirst;
          marks.user = aiFirst;
          const idx = boardState.findIndex(state => state === aiFirst);
          return post({ matchId, boardState, nextMove: { char: marks.user, position: idx } }).then(
            req => {
              expect(req.statusCode).to.be.equal(403);
              expect(req.body.message).to.be.equal('Not valid move');
              done();
            }
          );
        }

        const idx = randomIndex(0, 8);
        marks.ai = marks.user;
        return post({
          matchId,
          boardState,
          nextMove: { char: marks.user, position: idx },
        }).then(({ body: { boardState } }) => {
          const nextMove = { char: 'o', position: boardState.findIndex(state => state === 'o') };
          return post({ matchId, boardState, nextMove }).then(req => {
            expect(req.statusCode).to.be.equal(403);
            expect(req.body.message).to.be.equal('Not valid move');
            done();
          });
        });
      })
      .catch(done);
  });

  it('check invalid move trying to cheat with fake boardState', done => {
    match()
      .then(({ matchId, boardState, aiFirst }) => {
        boardState.push('-');

        return post({
          matchId,
          boardState,
          nextMove: { char: aiFirst === 'x' ? 'o' : 'x', position: 0 },
        }).then(req => {
          expect(req.statusCode).to.be.equal(403);
          expect(req.body.message).to.be.equal('Not valid move');
          done();
        });
      })
      .catch(done);
  });

  it('check invalid move trying to cheat with fake mark', done => {
    match()
      .then(({ matchId, boardState }) => {
        return post({
          matchId,
          boardState,
          nextMove: { char: 'random', position: 0 },
        }).then(req => {
          expect(req.statusCode).to.be.equal(403);
          expect(req.body.message).to.be.equal('Not valid move');
          done();
        });
      })
      .catch(done);
  });

  it('check invalid move trying to cheat with incorrect position', done => {
    match()
      .then(({ matchId, boardState, aiFirst }) => {
        return post({
          matchId,
          boardState,
          nextMove: { char: aiFirst === 'x' ? 'o' : 'x', position: -1 },
        }).then(req => {
          expect(req.statusCode).to.be.equal(403);
          expect(req.body.message).to.be.equal('Not valid move');
          done();
        });
      })
      .catch(done);
  });

  function randomIndex(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
});
