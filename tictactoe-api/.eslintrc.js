module.exports = {
  env: { jest: true },
  extends: 'airbnb-base',
  rules: {
    'comma-dangle': 0,
    'no-underscore-dangle': 0,
    'no-param-reassign': 0,
    'no-return-assign': 0,
    'no-use-before-define': 0,
    'arrow-parens': 0,
    'no-plusplus': 0,
    'no-shadow': 'off',
    'arrow-body-style': 'off',
    'guard-for-in': 'off',
    'no-restricted-syntax': 'off',
    camelcase: 0,
  },
};
