const { v4: uuidv4 } = require('uuid');
const express = require('express');

const router = express.Router();

const CELLS_COUNT = 9;
const MARKS = ['x', 'o'];
const WINNER_POSITIONS = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];
const memCache = {};

router.post('/play', (req, res) => {
  if (!validPayload(req)) return resError(res, 'INVALID_PAYLOAD');
  if (isEmptyPayload(req.body)) return res.json(createMatch());

  const { matchId } = req.body;
  if (!matchId || !memCache[matchId]) return resError(res, 'INVALID_MATCHID');
  if (memCache[matchId].finished) return resError(res, 'ALREADY_FINISHED');

  const { char, position } = req.body.nextMove;
  if (!memCache[matchId].aiData.mark) {
    memCache[matchId].aiData.mark = char === 'x' ? 'o' : 'x';
  }

  if (!validMove(req.body)) return resError(res, 'INVALID_MOVE');
  if (checkTie(matchId)) return resError(res, 'ALREADY_FINISHED');

  memCache[matchId].boardState[position] = char;
  memCache[matchId].aiData.current = true;

  const userWon = checkWinner({ char, matchId });
  if (userWon) return res.json({ ...req.body, ...sendWinner({ char, matchId, move: userWon }) });

  if (checkTie(matchId)) return resError(res, 'ALREADY_FINISHED');
  const aiMove = getAiMove(matchId);
  const aiChar = memCache[matchId].aiData.mark;
  memCache[matchId].boardState[aiMove] = aiChar;
  const aiWon = checkWinner({ char: aiChar, matchId });
  if (aiWon) {
    return res.json({ ...req.body, ...sendWinner({ char: aiChar, matchId, move: aiWon }) });
  }

  memCache[matchId].aiData.current = false;

  return res.json({ ...req.body, boardState: memCache[matchId].boardState, nextMove: null });
});

function resError(res, error) {
  let err = { code: 500, message: 'Unhandled error' };
  if (error === 'INVALID_PAYLOAD') err = { code: 403, message: 'Not valid payload format' };
  else if (error === 'INVALID_MATCHID') err = { code: 403, message: 'Not valid matchId' };
  else if (error === 'INVALID_MOVE') err = { code: 403, message: 'Not valid move' };
  else if (error === 'ALREADY_FINISHED') err = { code: 403, message: 'Match has finished' };

  return res.status(err.code).send({ error: true, message: err.message });
}

function isEmptyPayload(body) {
  // https://stackoverflow.com/a/59787784
  for (const i in body) return false;
  return true;
}

function validPayload(req) {
  try {
    return req && req.body && typeof req.body === 'object' && req.body !== null;
  } catch (err) {
    return true;
  }
}

function createMatch() {
  const matchId = uuidv4();
  const boardState = new Array(CELLS_COUNT).fill('-');
  const match = { matchId, boardState };

  const aiPlaysFirst = randomIndex(0, 1);
  memCache[matchId] = { ...match, aiData: { current: false } };
  if (aiPlaysFirst) {
    const aiMark = MARKS[randomIndex(0, 1)];
    boardState[randomIndex(0, CELLS_COUNT - 1)] = aiMark;
    memCache[matchId].aiData.mark = aiMark;
  }
  return match;
}

function validMove({ boardState: reqBoard, matchId, nextMove: { char, position } }) {
  const { boardState, aiData } = memCache[matchId];
  if (aiData.current || aiData.mark === char) return false;
  if (reqBoard.join('') !== boardState.join('')) return false;
  if (MARKS.indexOf(char) === -1) return false;
  if (position < 0 || position > CELLS_COUNT - 1) return false;
  return position >= 0 && boardState[position] === '-';
}

function checkTie(matchId) {
  const { boardState: board } = memCache[matchId];
  return !board.some(cell => cell !== 'x' && cell !== 'o');
}

function checkWinner({ char, matchId, minmax }) {
  const { boardState: board } = memCache[matchId];
  const winner = WINNER_POSITIONS.find(position => {
    const [tic, tac, toe] = position;
    return board[tic] === char && board[tac] === char && board[toe] === char;
  });

  if (!minmax && winner) memCache[matchId].finished = true;
  if (winner) return winner;
  return !board.some(cell => cell !== 'x' && cell !== 'o') ? 'tie' : null;
}

function getWinner(matchId) {
  let winner;
  MARKS.forEach(char => {
    if (winner) return;
    const maybeWinner = checkWinner({ char, matchId, minmax: true });
    if (maybeWinner) {
      winner = maybeWinner === 'tie' ? 'tie' : char;
    }
  });
  return winner;
}

function sendWinner({ char, matchId, move }) {
  return {
    nextMove: null,
    boardState: memCache[matchId].boardState,
    winner: { char, move },
  };
}

const SCORES = { tie: 0 };
function getAiMove(matchId) {
  const { boardState } = memCache[matchId];
  const { mark } = memCache[matchId].aiData;
  const userMark = mark === 'x' ? 'o' : 'x';
  SCORES[mark] = 10;
  SCORES[userMark] = -10;

  let bestScore = -Infinity;
  let move;
  for (let idx = 0; idx < CELLS_COUNT; idx++) {
    if (boardState[idx] === '-') {
      boardState[idx] = mark;
      const score = minimax(0, false);
      boardState[idx] = '-';
      if (score > bestScore) {
        bestScore = score;
        move = idx;
      }
    }
  }
  return move;

  function minimax(depth, isMaximizing) {
    const result = getWinner(matchId);
    if (result) return SCORES[result];

    if (isMaximizing) {
      let bestScore = -Infinity;
      for (let idx = 0; idx < CELLS_COUNT; idx++) {
        if (boardState[idx] === '-') {
          boardState[idx] = mark;
          const score = minimax(depth + 1, false);
          boardState[idx] = '-';
          bestScore = Math.max(score, bestScore);
        }
      }
      return bestScore;
    }

    let bestScore = Infinity;
    for (let idx = 0; idx < CELLS_COUNT; idx++) {
      if (boardState[idx] === '-') {
        boardState[idx] = userMark;
        const score = minimax(depth + 1, true);
        boardState[idx] = '-';
        bestScore = Math.min(score, bestScore);
      }
    }
    return bestScore;
  }
}

function randomIndex(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports = router;
