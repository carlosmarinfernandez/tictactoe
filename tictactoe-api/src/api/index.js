const express = require('express');

const tictactoe = require('./tictactoe/play');

const router = express.Router();

router.get('/', (req, res) => {
  res.json({ message: 'API - 👋🌎🌍🌏' });
});

router.use('/tic-tac-toe', tictactoe);

module.exports = router;
