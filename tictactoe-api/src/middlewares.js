function notFound(req, res, next) {
  res.status(404);
  const error = new Error(
    `Not Found '${req.originalUrl}'. Try '${req.headers.host}/api/tic-tac-toe/play'`
  );
  next(error);
}

/* eslint-disable no-unused-vars */
function errorHandler(err, req, res, next) {
  /* eslint-enable no-unused-vars */
  const statusCode = res.statusCode !== 200 ? res.statusCode : 500;
  res.status(statusCode);
  res.json({
    error: true,
    message: err.message,
    stack: process.env.NODE_ENV === 'production' ? '🥞' : err.stack,
  });
}

module.exports = {
  notFound,
  errorHandler,
};
