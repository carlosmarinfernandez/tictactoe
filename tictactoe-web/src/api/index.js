const api = {
  play: body => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    };

    return fetch('http://localhost:3000/api/tic-tac-toe/play', requestOptions).then(response =>
      response.json()
    );
  },
};

export default api;
