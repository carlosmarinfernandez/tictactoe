const storage = {
  get: key => {
    let value = null;
    try {
      value = sessionStorage.getItem(key);
      return JSON.parse(value);
    } catch (error) {
      return value;
    }
  },
  set: (key, value) => {
    try {
      let item = value;
      if (value && typeof value === 'object') item = JSON.stringify(value);
      return sessionStorage.setItem(key, item);
    } catch (error) {
      return null;
    }
  },
  remove: key => {
    sessionStorage.removeItem(key);
  },
};

export default storage;
