import * as React from 'react';
import api from '../api';
import storage from '../utils/storage/session';

const AppContext = React.createContext();

function AppProvider({ children }) {
  const [state, setState] = React.useState({});

  React.useEffect(() => {
    const match = storage.get('match');
    match ? updateState(match) : actions.restart();
  }, []);

  const actions = {
    restart: () => {
      clearState();
      api.play({}).then(data => {
        const { boardState } = data;
        const aiMove = boardState.find(state => state !== '-');
        if (aiMove) data.user = { mark: aiMove === 'o' ? 'x' : 'o', current: true };
        else data.user = null;
        updateState(data);
      });
    },
    selectMark: mark => {
      const copy = JSON.parse(JSON.stringify(state));
      if (copy.matchId) {
        if (!copy.user) copy.user = {};
        copy.user.mark = mark;
        copy.user.current = true;

        updateState(copy);
      }
    },
    sendMove: idx => {
      const { matchId, boardState, user } = JSON.parse(JSON.stringify(state));
      if (user) {
        api
          .play({ matchId, boardState, nextMove: { char: user.mark, position: idx }, winner: null })
          .then(data => updateState(data));
      }
    },
  };

  const value = { state, actions };
  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;

  function clearState() {
    setState(() => ({}));
    storage.remove('match');
  }

  function updateState(data) {
    setState(previous => ({ ...previous, ...data }));
    if (data && data.matchId) storage.set('match', data);
  }
}

function useApp() {
  const context = React.useContext(AppContext);
  if (context === undefined) throw new Error('useApp must be used within a AppProvider');
  return context;
}

export { AppProvider, useApp };
