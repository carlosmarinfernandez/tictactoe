import * as React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Game } from './components/game';
import { AppProvider } from './contexts/app-context';

function App() {
  return (
    <AppProvider>
      <Game />
    </AppProvider>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
