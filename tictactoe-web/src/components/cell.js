import { useApp } from '../contexts/app-context';

const Cell = ({ cellState, idx }) => {
  const { state, actions } = useApp();

  const handleClick = () => {
    if (cellState === '-' && !state.winner) actions.sendMove(idx);
  };

  let className = 'cell';
  if (state.user.mark !== cellState && state.user.mark !== '-') className += ' ai';
  if (cellState === 'x') className += ' cross';
  else if (cellState === 'o') className += ' circle';
  if (state.winner) {
    const { char, move } = state.winner;
    if (move === 'tie') className += ' tie';
    if (char === cellState && move.indexOf(idx) > -1) className += ' winner';
  }

  return <div className={className} onClick={handleClick} disabled={cellState !== '-'}></div>;
};

export { Cell };
