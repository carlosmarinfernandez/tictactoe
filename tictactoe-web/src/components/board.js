import * as React from 'react';
import { useApp } from '../contexts/app-context';
import { Cell } from './cell';

const Board = () => {
  const { state, actions } = useApp();

  return (
    <React.Fragment>
      <div className="board">
        {state.boardState
          ? state.boardState.map((cellState, idx) => {
              return <Cell key={idx} idx={idx} cellState={cellState}></Cell>;
            })
          : null}
      </div>

      <button className="btn m-t-5" onClick={actions.restart}>
        RESTART
      </button>
    </React.Fragment>
  );
};

export { Board };
