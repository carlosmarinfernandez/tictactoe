import * as React from 'react';
import { useApp } from '../contexts/app-context';
import { MarkSelector } from './mark-selector';
import { Board } from './board';

function Game() {
  const { state } = useApp();
  return <div className="game-box">{!state.user ? <MarkSelector /> : <Board />}</div>;
}

export { Game };
