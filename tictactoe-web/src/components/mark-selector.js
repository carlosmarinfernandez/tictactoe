import { useApp } from '../contexts/app-context';

const MarkSelector = () => {
  const { actions } = useApp();

  const handleClick = mark => {
    actions.selectMark(mark);
  };

  return (
    <div className="mark-selector">
      <h2>ELIGE TU MARCA</h2>
      <div className="button-group">
        <button className="btn" onClick={() => handleClick('x')}>X</button>
        <button className="btn" onClick={() => handleClick('o')}>O</button>
      </div>
    </div>
  );
};

export { MarkSelector };
